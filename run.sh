#! /bin/bash

mkdir ./processed

frames=$(ls -l ./frames | wc -l)

for i in $(seq 1 $frames)
do
  ascii-image-converter -b -d 170,50 ./frames/f-$i.png > ./processed/f-$i.txt
  echo $i
done

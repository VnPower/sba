#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
  int frames = 6576;

  for (int i = 1; i <= frames; i++) {
    FILE *f;
    char filename[80];

    int length = snprintf( NULL, 0, "%d", i );
    char* frame = malloc( length + 1 );
    snprintf( frame, length + 1, "%d", i );

    strcpy(filename, "./processed/f-");
    strcat(filename, frame);
    strcat(filename, ".txt");

    f = fopen(filename, "r");

    char c;
    while ((c = fgetc(f)) != EOF) {
      putchar(c);
    }

    fclose(f);

    usleep(32700);
  }
}
